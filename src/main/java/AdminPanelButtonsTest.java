import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

// Класс в ктором тестируются кнопки главного меню
public class AdminPanelButtonsTest extends Properties {

    public static void main(String[] args) {
        WebDriver driver = getDriver();
        driver.get(SITE_URL);
        driver.manage().window().maximize();
        //определяем элемент(локатор) для поля Эмаил, чистим Поле, вводим эмаил
        WebElement emailLocator = driver.findElement(By.id("email"));
        emailLocator.clear();
        emailLocator.sendKeys(LOGINEMAIL);
        //определяем элемент(локатор) для поля Пароль, чистим Поле, вводим пароль
        WebElement passwLocator = driver.findElement(By.id("passwd"));
        passwLocator.clear();
        passwLocator.sendKeys(PASSWORD);
        //определяем элемент(локатор) для кнопки, кликаем кнопку Войти
        WebElement submitButtonLocator = driver.findElement(By.name("submitLogin"));
        submitButtonLocator.click();
        sleep();
                //создаем массив локаторов кнопок Главного меню
        String[] buttonLocators = {"Dashboard", "Заказы", "Каталог","Клиенты","Служба поддержки",
                "Статистика","Modules","Design","Доставка","Способ оплаты","International","Shop Parameters"};
                //тестируем кнопки
        for(String sumLocator: buttonLocators){
            WebElement element = driver.findElement(By.linkText(sumLocator));
            methodTest(driver, element);
        }
        driver.close();
    }
}


