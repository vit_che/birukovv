import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/// Класс, содержащий статик переменные и статик методы
public class Properties {
    public static String SITE_URL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";  //адрес сайта
    public static String LOGINEMAIL = "webinar.test@gmail.com";                                       //логин
    public static String PASSWORD = "Xcg7299bnSmMuRLp9ITw";                                           //пароль

                    //метод вызова драйвера браузера
        public static WebDriver getDriver() {
        String driverPath = System.getProperty("user.dir") + "/.idea/driver/chromedriver.exe";
        if (driverPath == null)
            throw new NullPointerException("Path to Chrome is not specified!");
        System.setProperty("webdriver.chrome.driver", driverPath);
        return new ChromeDriver();
    }
                    //метод тетстирования Кнопок меню АдминПанели
    public static void methodTest(WebDriver driver, WebElement name) {
        name.click();                                                              //Кликаем кнопку Меню
        sleep();                                                                   //ожидаем
        String pageURLBeforeRefresh = driver.getCurrentUrl();                      //получаем УРЛ до обновления
        String pageTitle1BeforeRefresh = driver.getTitle();                        //получаем Тайтл до обновления
        System.out.println("Page title is: " + pageTitle1BeforeRefresh);           // выводим в консоль Тайтл
        driver.navigate().refresh();                                               //обновляем страницу
        sleep();
        String pageURLAfterRefresh = driver.getCurrentUrl();                        //получаем УРЛ после обносдения
        String pageTitleAfterRefresh = driver.getTitle();                           //получаем Тайтл после обновлдения
        System.out.println("Compare Page URL - " + pageURLAfterRefresh.equals(pageURLBeforeRefresh));     //сравниваем УРЛы
        System.out.println("Compare PageTitles - " + pageTitleAfterRefresh.equals(pageTitle1BeforeRefresh));  //сравниваем Тайтлы
        System.out.println("Finish Test page " + driver.getTitle());                 //конец теста страницы
        System.out.println("******************************************************");
    }

    public static void sleep() {                    //Метод ожидания
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}