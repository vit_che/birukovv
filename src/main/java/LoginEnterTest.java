import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//Класс в котором тестируется вход в Админпанель и выход
public class LoginEnterTest extends Properties {

    public static void main(String[] args) {

        WebDriver driver = getDriver();                 //Создаем драйвер
        driver.get(SITE_URL);                           //загружаем страницу
        driver.manage().window().maximize();
        //определяем элемент(локатор) для поля Эмаил, чистим Поле, вводим эмаил
        WebElement emailLocator = driver.findElement(By.id("email"));
        emailLocator.clear();
        emailLocator.sendKeys(LOGINEMAIL);
        //определяем элемент(локатор) для поля Пароль, чисим Поле, вводим пароль
        WebElement passwLocator = driver.findElement(By.id("passwd"));
        passwLocator.clear();
        passwLocator.sendKeys(PASSWORD);
        //определяем элемент(локатор) для кнопки, кликаем кнопку Войти
        WebElement submitButtonLocator = driver.findElement(By.name("submitLogin"));
        submitButtonLocator.click();
        sleep();
        //определяем элемент(локатор) пикторграммы, кликаем
        WebElement exitImgLocator = driver.findElement(By.xpath("//img[@width='32']"));
        exitImgLocator.click();
        //определяем элемент(локатор) кнопку "Выход", кликаем
        WebElement exitButton = driver.findElement(By.id("header_logout"));
        exitButton.click();

        driver.close();

    }
}
